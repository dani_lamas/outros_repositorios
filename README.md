# outros_repositorios
[GL] Listaxe de repositorios nos que participei.

[ES] Lista de repositorios en los que participé.

[EN] This is a list of public repositories to which I have contributed.

## Applied Geotechnologies Research Group

[https://github.com/GeoTechUVigo/VoxRasterLAS](https://github.com/GeoTechUVigo/VoxRasterLAS)

[https://github.com/GeoTechUVigo/cropmerge](https://github.com/GeoTechUVigo/cropmerge)

[https://github.com/GeoTechUVigo/synthetic_truss_bridges](https://github.com/GeoTechUVigo/synthetic_truss_bridges)

[https://github.com/GeoTechUVigo/truss_bridge_pc_segmentation_dl](https://github.com/GeoTechUVigo/truss_bridge_pc_segmentation_dl)

[https://github.com/GeoTechUVigo/truss_bridge_pointcloud_segmentation](https://github.com/GeoTechUVigo/truss_bridge_pointcloud_segmentation)

[https://github.com/GeoTechUVigo/las2laz](https://github.com/GeoTechUVigo/las2laz)
